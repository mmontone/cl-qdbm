(defpackage :qdbm.depot
  (:use :cl :assoc-utils))

(in-package :qdbm.depot)

(defparameter +dpopen-constants+
  (list (cons :create  qdbm.ffi.depot:+dp-ocreat+)
	(cons :locked qdbm.ffi.depot:+dp-olcknb+)
	(cons :unlocked qdbm.ffi.depot:+dp-onolck+)
	(cons :writer qdbm.ffi.depot:+dp-owriter+)
	(cons :reader qdbm.ffi.depot:+dp-oreader+)
	))

(defun dpopen-constant (key)
  (or (aget +dpopen-constants+ key)
      (error "Invalid key: ~a" key)))

(defun dpopen (name connection-mode &key (bnum 0) (if-does-not-exist :create))
  (check-type connection-mode (member :writer :reader))
  
  (let ((omode (if (eql connection-mode :writer)
		   (logior (dpopen-constant if-does-not-exist)
			   (dpopen-constant connection-mode))
		   (dpopen-constant connection-mode))))
    (let ((depot (qdbm.ffi.depot:dpopen name omode bnum)))
      (if (null depot)
	  (error "Error opening the database")
	  depot))))

(defun dpclose (depot)
  "DEPOT specifies a database handle."
  (qdbm.ffi.depot:dpclose depot))
