;;;; cl-qdbm.asd

(asdf:defsystem #:cl-qdbm
  :description "QDBM bindings for Common Lisp"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "qdbm")))
